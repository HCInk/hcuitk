[![language: C++](https://img.shields.io/badge/language-C%2B%2B-f34b7d)](https://en.wikipedia.org/wiki/C%2B%2B)
[![C++: 20](https://img.shields.io/badge/C%2B%2B-20-f34b7d)](https://en.cppreference.com/w/cpp/20)
[![GCC: gnu++2a](https://img.shields.io/badge/GCC-gnu%2B%2B2a-f34b7d)](https://www.gnu.org/software/gcc/projects/cxx-status.html#cxx2a)
[![build: cmake](https://img.shields.io/badge/build-cmake-89e051)](https://cmake.org/)
[![license: LGPLv3](https://img.shields.io/badge/license-LGPLv3-blue)](https://choosealicense.com/licenses/lgpl-3.0/)

# UI Toolkit

UI Toolkit (aka hcuitk) is libaray, based on cairo, made to build fast user-interfaces, with the focus on productivity, inspired by the blender-ui and others.

## Project Status

**Creation of base implementation:** Most basic functions and principles are currently implemented, productive usage of the project in the current status is not advised.

## Roadmap

- Getting the basic implementation done
- Support for non-X-based display-sysetems (aka Windows/Mac support)
- Multi-Windowing
- Multi language wrappers
