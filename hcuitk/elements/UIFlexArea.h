/*
 * FlexArea.h
 *
 *  Created on: Feb 9, 2020
 */

#ifndef HCUITK_ELEMENTS_UIFLEXAREA_H_
#define HCUITK_ELEMENTS_UIFLEXAREA_H_

#include <utility>
#include <vector>

#include "../hcuitk.h"
#include "UIArea.h"


namespace hcuitk {

enum FlexAreaAlign {
	FlexAreaAlign_START = 0,
	FlexAreaAlign_CENTER = 1,
	FlexAreaAlign_END = 2,
	FlexAreaAlign_SPACE_BETWEEN = 3
};

enum FlexAreaDirection {
	FlexAreaDirection_LEFT_RIGHT = 0,
	FlexAreaDirection_RIGHT_LEFT = 1,
	FlexAreaDirection_TOP_BOTTOM = 2,
	FlexAreaDirection_BOTTOM_TOP = 3
};

class UIFlexArea : public UIArea {

protected:
	FlexAreaAlign align;
	FlexAreaDirection direction;

	// Flag to (re)measure contained elements
	bool reMeasureSize = true;
	// Sum of all minimum sizes on the primary dimension
	int measuredTotalMinSize = -1;
	// Sum of all reommended sizes on the primary dimension
	int measuredTotalRecomSize = -1;
	// Count of elements that are growable (without recommended size)
	int measuredGrowables = -1;
	/*Calculated arrangement for the elements
	 * [i][0] = primary-min
	 * [i][1] = primary-recom
	 * [i][2] = secondary-min
	 * [i][3] = secondary-recom
	 */
	std::vector< std::vector<int> >* measuredSizes = NULL;



	// Internal Functions

	virtual void calculateArrangement();

	virtual void refreshSubDimensions() {
		reMeasureSize = true;
	}


public:
	UIFlexArea(std::vector<UIElement*>* elements, FlexAreaAlign align, FlexAreaDirection direction);
	virtual ~UIFlexArea();

	// Properties
	virtual std::pair<int, int>* getMinDimensions();
	virtual std::pair<int, int>* getRecomDimensions();
	virtual long dimensionVersion();

};

} /* namespace hcuitk */

#endif // HCUITK_ELEMENTS_UIFLEXAREA_H_
