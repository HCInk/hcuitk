/*
 * TestUIElement.h
 *
 *  Created on: Jan 26, 2020
 */

#ifndef HCUITK_ELEMENTS_TESTUIELEMENT_H_
#define HCUITK_ELEMENTS_TESTUIELEMENT_H_

#include <utility>

#include "../hcuitk.h"

namespace hcuitk {

class TestUIElement : public UISimpleElement {
private:
	long currentVersion = 0;
	long dimVersion = 0;
	int i = 0;
	int setPosX = 0;
	int setPosY = 0;
	int cursPosX = 0;
	int cursPosY = 0;

	std::pair<int, int>* minDims;
	std::pair<int, int>* recomDims;
public:
	TestUIElement();
	TestUIElement(std::pair<int, int>* minDims, std::pair<int, int>* recomDims);

	virtual ~TestUIElement();


	virtual void draw(cairo_t* cr, DrawingParameters* dp);
	virtual long version();

	virtual void pumpEvent(UIEvent* ev, bool explicitPush);

	virtual std::pair<int, int>* getMinDimensions();
	virtual std::pair<int, int>* getRecomDimensions();
	virtual long dimensionVersion();
};

} /* namespace hcuitk */

#endif /* HCUITK_ELEMENTS_TESTUIELEMENT_H_ */
