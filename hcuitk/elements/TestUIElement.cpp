/*
 * TestUIElement.cpp
 *
 *  Created on: Jan 26, 2020
 */

#include "TestUIElement.h"

#include <math.h>
#include <iostream>
#include <utility>

#define PI 3.14159265

namespace hcuitk {

TestUIElement::TestUIElement() {
	this->minDims = NULL;
	this->recomDims = NULL;
}

TestUIElement::TestUIElement(std::pair<int, int>* minDims, std::pair<int, int>* recomDims) {
	this->minDims = minDims;
	this->recomDims = recomDims;
}

TestUIElement::~TestUIElement() {
	delete minDims;
	delete recomDims;
}

void TestUIElement::draw(cairo_t* cr, DrawingParameters* dp) {

	int offX = dp->getOffX();
	int offY = dp->getOffY();

	double val = PI / 180;

	//draw some text
	cairo_select_font_face(cr, "serif", CAIRO_FONT_SLANT_NORMAL,
			CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 32.0);

	double x = (1 + sin(static_cast<double>(i % 360) * val)) / 2;
	double y = (1 + cos(static_cast<double> (i % (432)) * val / 1.2)) / 2;
	double c = (1 + cos(static_cast<double> (i % (468)) * val / 1.3)) / 2;

	cairo_set_source_rgb(cr, c, x, y);
	cairo_move_to(cr,
			offX+ (0.1 + x * 0.8) * dp->getWidth(),
			offY+ (0.1 + y * 0.8) * dp->getHeight()
		);

	int MAX_STR_LEN = 100;
	char Output[MAX_STR_LEN];
	snprintf(Output, MAX_STR_LEN, "%s%d", "HALLO", i);

	cairo_show_text(cr, Output);

	cairo_move_to(cr, 	offX+ setPosX,			offY+ 0);
	cairo_line_to(cr, 	offX+ setPosX, 			offY+ dp->getHeight());
	cairo_move_to(cr, 	offX+ 0, 				offY+ setPosY);
	cairo_line_to(cr, 	offX+ dp->getWidth(),	offY+ setPosY);

	cairo_set_line_width(cr, 2);
	cairo_stroke(cr);

	int width = dp->getWidth();
	int height = dp->getHeight();

	// Top Left
	int boundTLx, boundTLy;

	boundTLx = cursPosX - cursPosY;
	if (boundTLx < 0) {
		boundTLy = -boundTLx;
		boundTLx = 0;
	} else {
		boundTLy = 0;
	}

	// Bottom Right
	int boundBRx, boundBRy;

	boundBRx = cursPosX + (height - cursPosY);
	if (boundBRx > width) {
		boundBRy = height-(boundBRx-width);
		boundBRx = width;
	} else {
		boundBRy = height;
	}


	// Bottom Left
	int boundBLx, boundBLy;

	boundBLy = cursPosY + cursPosX;
	if (boundBLy > height) {
		boundBLx = boundBLy-height;
		boundBLy = height;
	} else {
		boundBLx = 0;
	}

	// Top Right
	int boundTRx, boundTRy;

	boundTRy = cursPosY - (width - cursPosX);
	if (boundTRy < 0) {
		boundTRx = width-(-boundTRy);
		boundTRy = 0;
	} else {
		boundTRx = width;
	}


	cairo_move_to(cr, 	offX+ boundTLx, 	offY+ boundTLy);
	cairo_line_to(cr, 	offX+ boundBRx,		offY+ boundBRy);
	cairo_move_to(cr, 	offX+ boundBLx, 	offY+ boundBLy);
	cairo_line_to(cr, 	offX+ boundTRx, 	offY+ boundTRy);

	cairo_set_line_width(cr, 1);
	cairo_stroke(cr);

	i++;
}

long TestUIElement::version() {
	return currentVersion;
}

void TestUIElement::pumpEvent(UIEvent* ev, bool explicitPush) {
	switch (ev->getType()) {
	case UIEventType::Event_CURSOR_PRESS: {
		UICurserButtonEvent* butEv = reinterpret_cast<UICurserButtonEvent*>(ev);
		setPosX = butEv->getCursorX();
		setPosY = butEv->getCursorY();

		currentVersion++;
		sendUpdate(false);


		UIMessageCommand msg("Test", setPosX, setPosY);
		sendUpCommand(&msg);

		break;
	}

	case UIEventType::Event_CURSER_MOTION: {
		UICurserMoveEvent* motEv = reinterpret_cast<UICurserMoveEvent*>(ev);
		cursPosX = motEv->getCursorX();
		cursPosY = motEv->getCursorY();

		currentVersion++;
		sendUpdate(false);
	}
		break;
	default:
		break;
	}
}

std::pair<int, int>* TestUIElement::getMinDimensions() {
	return minDims;
}

std::pair<int, int>* TestUIElement::getRecomDimensions() {
	if (recomDims != NULL) {
		if (recomDims->first == 33) {
			recomDims->second = 200 + static_cast<double>(sin( static_cast<double>(i % 36 *10)*(PI / 180) )* 50.0);
			dimVersion++;
		}
	}
	return recomDims;
}


long TestUIElement::dimensionVersion() {
	if (recomDims != NULL) {
		if (recomDims->first == 33) {
			return -1;
		}
	}
	return 0;
}

} /* namespace hcuitk */
