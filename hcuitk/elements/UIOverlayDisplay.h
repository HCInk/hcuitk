/*
 * UIOverlayDisplay.h
 *
 *  Created on: Feb 29, 2020
 */

#ifndef HCUITK_ELEMENTS_UIOVERLAYDISPLAY_H_
#define HCUITK_ELEMENTS_UIOVERLAYDISPLAY_H_

#include "../hcuitk.h"
#include "UIArea.h"

namespace hcuitk {

class UIOverlayElement : public UIElement {
protected:
	UIElement* element;
	std::pair<int, int>* pos;

public:

	UIOverlayElement(UIElement* element, std::pair<int, int>* pos);

	virtual ~UIOverlayElement();

	virtual std::pair<int, int>* getPos();

	// Core Functions

	virtual void draw(cairo_t* cr, DrawingParameters* dp);

	virtual long version();

	// Registration

	virtual void registerElement(UIHolderInterface* uiHolder, std::vector<UIElement*>* path);

	virtual void unregisterElement(bool notify=true);


	// Events

	// Downstream-Events
	virtual bool pumpEvent(UIEvent* ev);
	virtual bool pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex);
	// Upstream-Events
	virtual void notifyUpdate(bool dimensions, UIElement* cause = NULL);
	virtual void pumpCommand(UICommand* cmd, UIElement* source);

	// Properties

	virtual std::pair<int, int>* getMinDimensions();

	virtual std::pair<int, int>* getRecomDimensions();

	virtual long dimensionVersion();

};

class UIOverlayDisplay : public UIArea {
protected:
	// Internal Functions

	virtual void calculateArrangement();

	virtual void addElement(UIOverlayElement* elem) ;
	virtual void removeElement(UIOverlayElement* elem);

public:
	UIOverlayDisplay();
	virtual ~UIOverlayDisplay();

	virtual void pumpCommand(UICommand* cmd, UIElement* source);

};

} /* namespace hcuitk */

#endif /* HCUITK_ELEMENTS_UIOVERLAYDISPLAY_H_ */
