/*
 * FlexArea.cpp
 *
 *  Created on: Feb 9, 2020
 */

#include "UIFlexArea.h"

#include <iostream>
#include <vector>
#include <utility>

namespace hcuitk {

UIFlexArea::UIFlexArea(
		std::vector<UIElement*>* elements,
		FlexAreaAlign align,
		FlexAreaDirection direction
		) : UIArea(elements) {
	this->align = align;
	this->direction = direction;
}

UIFlexArea::~UIFlexArea() {
	if (measuredSizes != NULL) {
		delete measuredSizes;
	}
}


// Internal

void UIFlexArea::calculateArrangement() {

	std::cout << "calc arr" << std::endl;

	//
	// Prepare / Evaluate
	//

	int elementCount = elements->size();

	if (calculatedArrangement != NULL) {
		delete calculatedArrangement;
	}

	calculatedArrangement = new std::vector< std::vector<int> >(elementCount, std::vector<int>(4));

	bool vertical;
	bool antiAligned;

	switch (direction) {
	case FlexAreaDirection_LEFT_RIGHT:
		antiAligned = false;
		vertical = false;
		break;
	case FlexAreaDirection_RIGHT_LEFT:
		antiAligned = true;
		vertical = false;
		break;
	case FlexAreaDirection_TOP_BOTTOM:
		antiAligned = false;
		vertical = true;
		break;
	case FlexAreaDirection_BOTTOM_TOP:
		antiAligned = true;
		vertical = true;
		break;
	default:
		vertical = false;
		antiAligned = false;
		break;
	}

	// Available size of primary dimension
	int availableSize;
	// Available size of secondary dimension
	int secondaryAvailableSize;

	if (vertical) {
		availableSize = currentHeight;
		secondaryAvailableSize = currentWidth;
	} else {
		availableSize = currentWidth;
		secondaryAvailableSize = currentHeight;
	}

	//
	// Measure element-sizes
	//

	if (reMeasureSize) {
		reMeasureSize = false;


		if (measuredSizes != NULL) {
			delete measuredSizes;
		}

		measuredSizes = new std::vector< std::vector<int> >(elementCount, std::vector<int>(4, -1));

		measuredTotalMinSize = 0;
		measuredTotalRecomSize = 0;
		measuredGrowables = 0;

		if (vertical) {// Verical Measurements

			for(int i = 0; i < elementCount; i++) {
				UIElement* elem = (*elements)[i];
				std::pair<int, int>* minDims = elem->getMinDimensions();
				std::pair<int, int>* recomDims = elem->getRecomDimensions();
				int minSize;
				if (minDims != NULL) {
					minSize = minDims->second;
					// primary-min
					(*measuredSizes)[i][0] = minDims->second;
					// secondary-min
					(*measuredSizes)[i][2] = minDims->first;
				} else {
					minSize = 0;
					// primary-min
					(*measuredSizes)[i][0] = 0;
					// secondary-min
					(*measuredSizes)[i][2] = 0;
				}
				// total-min
				measuredTotalMinSize += minDims->second;

				if (recomDims != NULL) {
					int recomSize = recomDims->second;
					if (recomSize > 0) {
						// total-recom
						measuredTotalRecomSize += recomSize;
					} else {
						measuredTotalRecomSize += minSize;
						measuredGrowables++;
					}
					// primary-recom
					(*measuredSizes)[i][1] = recomDims->second;
					// secondary-recom
					(*measuredSizes)[i][3] = recomDims->first;
				} else {
					measuredTotalRecomSize += minSize;
					measuredGrowables++;
				}
			}

		} else {// Horizontal Measurements

			for(int i = 0; i < elementCount; i++) {
				UIElement* elem = (*elements)[i];
				std::pair<int, int>* minDims = elem->getMinDimensions();
				std::pair<int, int>* recomDims = elem->getRecomDimensions();

				int minSize;
				if (minDims != NULL) {
					minSize = minDims->first;
					// primary-min
					(*measuredSizes)[i][0] = minDims->first;
					// secondary-min
					(*measuredSizes)[i][2] = minDims->second;
				} else {
					minSize = 0;
					// primary-min
					(*measuredSizes)[i][0] = 0;
					// secondary-min
					(*measuredSizes)[i][2] = 0;
				}
				// total-min
				measuredTotalMinSize += minSize;

				if (recomDims != NULL) {
					int recomSize = recomDims->first;
					if (recomSize > 0) {
						// total-recom
						measuredTotalRecomSize += recomSize;
					} else {
						measuredTotalRecomSize += minSize;
						measuredGrowables++;
					}
					// primary-recom
					(*measuredSizes)[i][1] = recomDims->first;
					// secondary-recom
					(*measuredSizes)[i][3] = recomDims->second;
				} else {
					measuredTotalRecomSize += minSize;
					measuredGrowables++;
				}
			}

		}
	}

	//
	// Linear Sizing
	//

	// Space between the elements
	double spaceLeft;
	// Ammount how elements without any recommended size will grow
	double growFactor;
	 // Factor much of the possible expainsion is should be done
	double expansionFactor;
	{
		int extraSize = availableSize-measuredTotalRecomSize;
		if (extraSize > 0) {

			expansionFactor = 1;

			if (measuredGrowables > 0) {
				spaceLeft = 0.0;
				growFactor = static_cast<double>(extraSize)/measuredGrowables;
			} else {
				spaceLeft = static_cast<double>(extraSize);
				growFactor = 0.0;
			}

		} else {

			growFactor = 0.0;
			spaceLeft = 0.0;
			int expainsionAmmount = measuredTotalRecomSize-measuredTotalMinSize;
			if (expainsionAmmount > -extraSize) {
				expansionFactor = 1.0-(static_cast<double>(-extraSize)/expainsionAmmount);
			} else {
				expansionFactor = 0.0;
			}

		}

	}

	//
	// Placing
	//

	// Current Position of placer
	double currentPos = 0;

	// Helper variable for FlexAreaMode_SPACE_BETWEEN
	double spaceBetween;

	switch (align) {
	case FlexAreaAlign_CENTER:
		currentPos = spaceLeft/2.0;
		break;

	case FlexAreaAlign_END:
		currentPos = spaceLeft;
		break;

	case FlexAreaAlign_SPACE_BETWEEN:
		if (elementCount > 1) {
			spaceBetween = spaceLeft/(elementCount-1);
		}
		break;

	default:
		break;
	}

	for(int i = 0; i < elementCount; i++) {
		// Size
		double minSize = static_cast<double>((*measuredSizes)[i][0]); // primary-min
		double recomSize = static_cast<double>((*measuredSizes)[i][1]); // primary-recom
		int recomSecSize = (*measuredSizes)[i][3]; // secondary-recom

		double size;
		int secondarySize;

		if (recomSize >= 0) {
			size = minSize+(expansionFactor*(recomSize-minSize));
			secondarySize = (recomSecSize < 0 || recomSecSize > secondaryAvailableSize) ? secondaryAvailableSize : recomSecSize;
		} else {
			size = minSize+growFactor;
			secondarySize = secondaryAvailableSize;
		}

		if (vertical) {
			(*calculatedArrangement)[i][3] = static_cast<int>(size); // primary-size: height
			(*calculatedArrangement)[i][2] = secondarySize; // secondary-size: width
		} else {
			(*calculatedArrangement)[i][2] = static_cast<int>(size); // primary-size: width
			(*calculatedArrangement)[i][3] = secondarySize; // secondary-size: height
		}

		// Position

		int pos;

		switch (align) {

		case FlexAreaAlign_SPACE_BETWEEN:
			if (i > 0 && i < elementCount) {
				currentPos += spaceBetween;
			}
			break;

		default:
			break;
		}

		pos = currentPos;
		currentPos += size;

		if (vertical) {

			if (antiAligned) {
				int antiPos = availableSize-pos-size;
				(*calculatedArrangement)[i][1] = static_cast<int>(antiPos); // primary-pos: y
			} else {
				(*calculatedArrangement)[i][1] = static_cast<int>(pos); // primary-pos: y
			}

			(*calculatedArrangement)[i][0] = 0; // secondary-pos: x

		} else {

			if (antiAligned) {
				int antiPos = availableSize-pos-size;
				(*calculatedArrangement)[i][0] = static_cast<int>(antiPos); // primary-pos: x
			} else {
				(*calculatedArrangement)[i][0] = static_cast<int>(pos); // primary-pos: x
			}

			(*calculatedArrangement)[i][1] = 0; // secondary-pos: y

		}

	}

}




// Properties

//TODO Dimensions

std::pair<int, int>* UIFlexArea::getMinDimensions() {
	return NULL;
}

std::pair<int, int>* UIFlexArea::getRecomDimensions() {
	return NULL;
}

long UIFlexArea::dimensionVersion() {
	return 0;
}



} /* namespace hcuitk */
