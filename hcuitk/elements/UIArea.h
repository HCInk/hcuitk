/*
 * UIArea.h
 *
 *  Created on: Feb 15, 2020
 */

#ifndef HCUITK_ELEMENTS_UIAREA_H_
#define HCUITK_ELEMENTS_UIAREA_H_

#include <utility>
#include <vector>

#include "../hcuitk.h"

namespace hcuitk {

class UIArea : public UIElement {
private:
	int selfVersion = 0;
	int dimVersion = 0;
	// 0: No update, 1: Content update, 2: Re measure
	int continuusUpdate = 0;

protected:

	std::vector<UIElement*>* elements;

	int currentWidth = -1;
	int currentHeight = -1;

	// Versions of the elements when they've been measured
	//std::vector< long >* lastVersions = NULL;

	/*Calculated arrangement for the elements
		 * [i][0] = x-cord
		 * [i][1] = y-cord
		 * [i][2] = with
		 * [i][3] = height
		 */
	std::vector< std::vector<int> >* calculatedArrangement = NULL;

	inline std::vector< std::vector<int> >* getArrangement() {
		if (calculatedArrangement == NULL) {
			calculateArrangement();
		}
		return calculatedArrangement;
	}

	// Gets called if own or sub-dimensions get changed, to refresh the calculated arrangement
	inline void refreshArrangementCalc() {
		if (calculatedArrangement != NULL) {
			delete calculatedArrangement;
			calculatedArrangement = NULL;
		}
	}

	// Finds the element with a specific address
	virtual std::pair<UIElement*, std::vector<int>> findElement(UIElement* elem);
	// Finds the element at a specific position
	virtual std::pair<UIElement*, std::vector<int>> findElement(int x, int y);

	inline void updateSelf() {
		selfVersion++;
		sendUpdate(false);
	}

	inline void updateDims() {
		dimVersion++;
		sendUpdate(true);
	}

	virtual void checkVersions();

	virtual void drawElements(cairo_t* cr, DrawingParameters* dp);

	virtual UICommand* alignCommand(UICommand* cmd, UIElement* source);

	// Gets called when sub-elements have changed
	virtual void refreshElememts() {
		// Default: Refresh dimensions
		refreshSubDimensions();
	}

	// Gets called when sub-dimensions have changed
	virtual void refreshSubDimensions() {}

	// Method to generate the calculated-arrangement
	virtual void calculateArrangement() = 0;



public:
	explicit UIArea(std::vector<UIElement*>* elements) {
		this->elements = elements;
	}
	
	virtual ~UIArea() {
		if (calculatedArrangement != NULL) {
			delete calculatedArrangement;
		}
		delete elements;
	}

	virtual std::vector<UIElement*>* getElements() {
		return elements;
	}

	virtual void updateElements(std::vector<UIElement*>* elements);

	// Core Functions
	virtual void draw(cairo_t* cr, DrawingParameters* dp) {
		checkVersions();
		drawElements(cr, dp);
	}

	virtual long version() {
		return continuusUpdate == 0 ? selfVersion : -1;
	}

	// Registration
	virtual void registerElement(UIHolderInterface* uiHolder,
			std::vector<UIElement*>* path);
	virtual void unregisterElement(bool notify);

	// Events
	virtual bool pumpEvent(UIEvent* ev);
	virtual bool pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex);
	virtual void pumpCommand(UICommand* cmd, UIElement* source);
	virtual void notifyUpdate(bool dimensions, UIElement* cause);
};

} /* namespace hcuitk */

#endif // HCUITK_ELEMENTS_UIAREA_H_
