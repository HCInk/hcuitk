/*
 * UIOverlayHolder.h
 *
 *  Created on: Feb 29, 2020
 */

#ifndef HCUITK_ELEMENTS_UIOVERLAYHOLDER_H_
#define HCUITK_ELEMENTS_UIOVERLAYHOLDER_H_

#include "../hcuitk.h"

namespace hcuitk {

class UIOverlayManager : public UIElement {
private:
	UIElement* rootElement;
	UIElement* overlayElement;
	long currentVersion = 0;
public:
	UIOverlayManager(UIElement* rootElement, UIElement* overlayElement);
	virtual ~UIOverlayManager();

	// Core Functions

	virtual void draw(cairo_t* cr, DrawingParameters* dp);
	virtual long version();

	// Registration

	virtual void registerElement(UIHolderInterface* uiHolder, std::vector<UIElement*>* path);
	virtual void unregisterElement(bool notify=true);


	// Events

	// Downstream-Events
	virtual bool pumpEvent(UIEvent* ev);
	virtual bool pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex);
	// Upstream-Events
	virtual void notifyUpdate(bool dimensions, UIElement* cause = NULL);
	virtual void pumpCommand(UICommand* cmd, UIElement* source);
};

} /* namespace hcuitk */

#endif /* HCUITK_ELEMENTS_UIOVERLAYHOLDER_H_ */
