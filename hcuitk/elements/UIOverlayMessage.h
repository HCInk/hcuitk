/*
 * UIOverlayMessage.h
 *
 *  Created on: Apr 5, 2020
 */

#ifndef HCUITK_ELEMENTS_UIOVERLAYMESSAGE_H_
#define HCUITK_ELEMENTS_UIOVERLAYMESSAGE_H_

#include <string>

#include "../hcuitk.h"

namespace hcuitk {

class UIOverlayMessage : public UISimpleElement {
private:
	std::pair<int, int> dimmensions;
	std::string msg;
public:
	UIOverlayMessage(std::string msg);
	virtual ~UIOverlayMessage();


	virtual void draw(cairo_t* cr, DrawingParameters* dp);
	virtual long version();

	virtual void pumpEvent(UIEvent* ev, bool explicitPush);

	virtual std::pair<int, int>* getMinDimensions();
};

} /* namespace hcuitk */

#endif /* HCUITK_ELEMENTS_UIOVERLAYMESSAGE_H_ */
