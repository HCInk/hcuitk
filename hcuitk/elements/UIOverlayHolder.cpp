/*
 * UIOverlayHolder.cpp
 *
 *  Created on: Feb 29, 2020
 */

#include "UIOverlayHolder.h"

#include <iostream>

namespace hcuitk {


UIOverlayManager::UIOverlayManager(UIElement* rootElement, UIElement* overlayElement) {
	this->rootElement = rootElement;
	this->overlayElement = overlayElement;
}
UIOverlayManager::~UIOverlayManager() {

}

// Core Functions

void UIOverlayManager::draw(cairo_t* cr, DrawingParameters* dp) {
	rootElement->draw(cr, dp);
	overlayElement->draw(cr, dp);
}
long UIOverlayManager::version() {
	return currentVersion;
}

// Registration

void UIOverlayManager::registerElement(UIHolderInterface* uiHolder, std::vector<UIElement*>* path) {
	UIElement::registerElement(uiHolder, path);
	std::vector<UIElement*>* ownPath = getOwnPath();
	rootElement->registerElement(uiHolder, ownPath);
	overlayElement->registerElement(uiHolder, ownPath);
}

void UIOverlayManager::unregisterElement(bool notify) {
	UIElement::unregisterElement(notify);
	rootElement->unregisterElement(notify);
	overlayElement->unregisterElement(notify);
}

// Events

// Downstream-Events
bool UIOverlayManager::pumpEvent(UIEvent* ev) {
	bool overlayHit = overlayElement->pumpEvent(ev);
	if (overlayHit) {
		return true;
	} else {
		return rootElement->pumpEvent(ev);
	}
}
bool UIOverlayManager::pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex) {
	UIElement* selected = (*path)[pathIndex];
	if (selected == rootElement) {
		return rootElement->pumpEventExplicit(ev, path, pathIndex+1);
	} else if (selected == overlayElement) {
		return rootElement->pumpEventExplicit(ev, path, pathIndex+1);
	} else {
		return false;
	}
}
// Upstream-Events
void UIOverlayManager::notifyUpdate(bool dimensions, UIElement* cause) {
	this->currentVersion++;
	sendUpdate(dimensions);
}

void UIOverlayManager::pumpCommand(UICommand* cmd, UIElement* source) {
	std::cout << "CMD@UIOverlay" << std::endl;
	overlayElement->pumpCommand(cmd, source);
}

} /* namespace hcuitk */
