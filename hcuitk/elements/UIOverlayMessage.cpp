/*
 * UIOverlayMessage.cpp
 *
 *  Created on: Apr 5, 2020
 */

#include "UIOverlayMessage.h"

#include <iostream>
#include <string>

namespace hcuitk {



UIOverlayMessage::UIOverlayMessage(std::string msg) {
	this->msg = msg;
	dimmensions.first = 100;
	dimmensions.second = 20;
}

UIOverlayMessage::~UIOverlayMessage() {

}

void UIOverlayMessage::draw(cairo_t* cr, DrawingParameters* dp) {


	int x = dp->getOffX();
	int y = dp->getOffY();
	int width = dp->getWidth();
	int height = dp->getHeight();

	std::cout << "Draw message"
			"X" << x << "Y" << y << "W" << width << "H" << height << std::endl;

	cairo_set_source_rgba(cr, 0.1, 0.1, 0.1, 0.9);
	cairo_rectangle(cr, x, y, width, height);
	cairo_fill(cr);

	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_move_to(cr, x, y+height-3);
	cairo_select_font_face(cr, "serif", CAIRO_FONT_SLANT_NORMAL,
			CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 16.0);
	cairo_show_text(cr, msg.c_str());

}

long UIOverlayMessage::version() {
	return 0;
}

void UIOverlayMessage::pumpEvent(UIEvent* ev, bool explicitPush) {
	if (ev->getType() == Event_CURSOR_PRESS) {

		UICloseCommand closeCmd;
		sendUpCommand(&closeCmd);
	}
}

std::pair<int, int>* UIOverlayMessage::getMinDimensions() {
	return &dimmensions;
}

} /* namespace hcuitk */
