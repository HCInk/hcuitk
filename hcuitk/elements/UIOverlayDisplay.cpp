/*
 * UIOverlayDisplay.cpp
 *
 *  Created on: Feb 29, 2020
 */

#include "UIOverlayDisplay.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

#include "UIOverlayMessage.h"

namespace hcuitk {


UIOverlayElement::UIOverlayElement(UIElement* element, std::pair<int, int>* pos) {
	this->element = element;
	this->pos = pos;
}

UIOverlayElement::~UIOverlayElement() {
	delete element;
	delete pos;
}

std::pair<int, int>* UIOverlayElement::getPos() {
	return pos;
}


void UIOverlayElement::draw(cairo_t* cr, DrawingParameters* dp) {
	element->draw(cr, dp);
}

long UIOverlayElement::version() {
	return element->version();
}

void UIOverlayElement::registerElement(UIHolderInterface* uiHolder, std::vector<UIElement*>* path) {
	UIElement::registerElement(uiHolder, path);
	element->registerElement(this->uiHolder, getOwnPath());
}

void UIOverlayElement::unregisterElement(bool notify) {
	UIElement::unregisterElement(notify);
	element->unregisterElement(notify);
}


// Events

// Downstream-Events
bool UIOverlayElement::pumpEvent(UIEvent* ev) {
	return element->pumpEvent(ev);
}
bool UIOverlayElement::pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex) {
	return element->pumpEventExplicit(ev, path, pathIndex+1);
}
// Upstream-Events
void UIOverlayElement::notifyUpdate(bool dimensions, UIElement* cause) {
	sendUpdate(dimensions);
}

void UIOverlayElement::pumpCommand(UICommand* cmd, UIElement* source) {
	sendUpCommand(cmd);
}

std::pair<int, int>* UIOverlayElement::getMinDimensions() {
	return element->getMinDimensions();
}

std::pair<int, int>* UIOverlayElement::getRecomDimensions() {
	return element->getRecomDimensions();
}

long UIOverlayElement::dimensionVersion() {
	return element->dimensionVersion();
}


UIOverlayDisplay::UIOverlayDisplay() : UIArea(new std::vector<UIElement*>(0)) {
}

UIOverlayDisplay::~UIOverlayDisplay() {
	std::vector<UIElement*>* elems = getElements();
	for (UIElement* elem : *elems) {
		delete elem;
	}
}

void UIOverlayDisplay::calculateArrangement() {
	int elementCount = elements->size();

	if (calculatedArrangement != NULL) {
		delete calculatedArrangement;
	}

	calculatedArrangement = new std::vector< std::vector<int> >(elementCount, std::vector<int>(4));

	for(int i = 0; i < elementCount; i++) {
		UIOverlayElement* currentElement = static_cast<UIOverlayElement*> ((*elements)[i]);
		std::pair<int, int>* pos = currentElement->getPos();
		std::pair<int, int>* minDims = currentElement->getMinDimensions();
		std::pair<int, int>* recomDims = currentElement->getRecomDimensions();

		(*calculatedArrangement)[i][0] = pos->first;
		(*calculatedArrangement)[i][1] = pos->second;

		if (recomDims != NULL) {
			(*calculatedArrangement)[i][2] = recomDims->first;
			(*calculatedArrangement)[i][3] = recomDims->second;
		} else {
			if (minDims) {
				(*calculatedArrangement)[i][2] = minDims->first;
				(*calculatedArrangement)[i][3] = minDims->second;
			} else {
				std::cerr << "Overlay-element \"" << typeid(currentElement).name() << "\" does not provide any dimensions!" << std::endl;
				(*calculatedArrangement)[i][2] = 0;
				(*calculatedArrangement)[i][3] = 0;
			}
		}
	}
}

void UIOverlayDisplay::pumpCommand(UICommand* incmd, UIElement* source) {
	std::cout << "COMMAND REC DISP" << std::endl;

	hcuitk::UICommand* cmd;

	hcuitk::UICommand* alignedCommand = alignCommand(incmd, source);

	if (alignedCommand != NULL) {
		cmd = alignedCommand;
	} else {
		cmd = incmd;
	}

	if (UIMessageCommand* msgcmd = dynamic_cast<UIMessageCommand*>(cmd)) {

		UIOverlayMessage* overlay = new UIOverlayMessage(msgcmd->getMessage());

		std::cout << std::dec
			<< "put msg "
			<< msgcmd->getCursorX()
			<< " "
			<< msgcmd->getCursorY()
			<< std::endl;

		addElement(new UIOverlayElement(overlay,
			new std::pair<int, int>(msgcmd->getCursorX(), msgcmd->getCursorY()))
		);
	} else if (dynamic_cast<UICloseCommand*>(cmd)) {

		UIOverlayElement* srcElem = static_cast<UIOverlayElement*>(source);

		removeElement(srcElem);

		delete srcElem;

	} else {
		std::cerr << "Command not recognized by overlay!" << std::endl;
	}

	if (alignedCommand != NULL) {
		delete alignedCommand;
	}



}

void UIOverlayDisplay::addElement(UIOverlayElement* elem) {
	std::vector<hcuitk::UIElement*>* nelements = new std::vector<hcuitk::UIElement*>(*(getElements()));

	nelements->insert(nelements->end(), elem);

	updateElements(nelements);
}

void UIOverlayDisplay::removeElement(UIOverlayElement* elem) {
	std::vector<hcuitk::UIElement*>* nelements = new std::vector<hcuitk::UIElement*>(*(getElements()));

	std::vector<hcuitk::UIElement*>::iterator found = std::find(nelements->begin(), nelements->end(), elem);

	nelements->erase(found);

	updateElements(nelements);

}

} /* namespace hcuitk */
