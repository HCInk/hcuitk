/*
 * UIArea.cpp
 *
 *  Created on: Feb 15, 2020
 */

#include "UIArea.h"

#include <iostream>
#include <utility>
#include <vector>
#include <map>

namespace hcuitk {

std::pair<UIElement*, std::vector<int>> UIArea::findElement(UIElement* elem) {

	std::vector< std::vector<int> >* arrangement = getArrangement();

	int elemCount = elements->size();

	for (int i = 0; i < elemCount; i++) {

		UIElement* foundElem = (*elements)[i];

		if (foundElem == elem) {
			std::vector<int> foundVec = (*arrangement)[i];
			return std::pair<UIElement*, std::vector<int>>(foundElem, foundVec);
		}

	}

	return std::pair<UIElement*, std::vector<int>>(NULL, std::vector<int>());

}

std::pair<UIElement*, std::vector<int>> UIArea::findElement(int x, int y) {

	std::vector< std::vector<int> >* arrangement = getArrangement();

	int elemCount = elements->size();


	//std::cout << std::dec << "SEARCHING@X" << x << "Y" << y << std::endl;
	for (int i = elemCount-1; i >= 0; i--) {

		std::vector<int> foundVec = (*arrangement)[i];

		int elemX = foundVec[0];
		int elemY = foundVec[1];

		//std::cout << std::dec << "~ SEARCH" << i << " X" << elemX << "Y" << elemY << " " << foundVec[2] << "x" << foundVec[3] << std::endl;


		if (elemX <= x && elemY <= y && elemX+foundVec[2] > x && elemY+foundVec[3] > y) {
			//std::cout << "~ FOUND!" << std::endl;
			UIElement* foundElem = (*elements)[i];
			return std::pair<UIElement*, std::vector<int>>(foundElem, foundVec);
		}

	}

	return std::pair<UIElement*, std::vector<int>>(NULL, std::vector<int>());

}


void UIArea::checkVersions() {
	int continuusUpdate = 0;

	int elemCount = elements->size();
	for (int i = 0; i < elemCount; i++) {
		if ((*elements)[i]->dimensionVersion() < 0) {
			continuusUpdate = 2; // Remeasure Size
			break;
		}
		if (continuusUpdate < 1 && (*elements)[i]->version() < 0) {
			continuusUpdate = 1; // Update Content
			break;
		}
	}

	//std::cout << std::dec << "CHECK VERS "  << continuusUpdate << std::endl;

	this->continuusUpdate = continuusUpdate;

	if (continuusUpdate >= 2) {
		refreshSubDimensions();
		refreshArrangementCalc();
	}

}


void UIArea::drawElements(cairo_t* cr, DrawingParameters* dp) {
	int cWidth = dp->getWidth();
	int cHeight = dp->getHeight();
	if (currentWidth != cWidth || currentHeight != cHeight) {
		currentWidth = cWidth;
		currentHeight = cHeight;
		refreshArrangementCalc();
	}

	std::vector<std::vector<int>>* arrangement = getArrangement();

	int elementCount = elements->size();

	for(int i = 0; i < elementCount; i++) {
		UIElement* elem = (*elements)[i];
		int x = (*arrangement)[i][0]+dp->getOffX();
		int y = (*arrangement)[i][1]+dp->getOffY();
		int width = (*arrangement)[i][2];
		int height = (*arrangement)[i][3];


		//std::cout << "DAW-SUB" << std::dec << i << " X" << x << "Y" << y << " " << width << "x" << height << std::endl;

		cairo_rectangle(cr, x, y, width, height);
		cairo_clip(cr);

		DrawingParameters dp(x, y, width, height);

		elem->draw(cr, &dp);
		cairo_reset_clip(cr);

	}

}

void UIArea::updateElements(std::vector<UIElement*>* elements) {

	if (this->elements != elements) {
		int newCount = elements->size();
		bool unset[newCount];

		std::fill_n(unset, newCount, true);

		std::vector<UIElement*>::iterator newIt = elements->begin();

		std::cout << "REPLACEMENT OPERATION: " << std::endl;

		for (UIElement* elemOld : *this->elements) {
			//std::cout << "~ CHECK OLD 0x" << std::hex << elemOld << std::endl;
			bool refound = false;
			for (int i = 0; i < newCount; i++) {
				//std::cout << "  ~ COMP NEW 0x" << std::hex << newIt[i] << std::boolalpha << "(" << unset[i] << ")" << std::endl;
				if (unset[i] && newIt[i] == elemOld) {
					// Item re-found in new vector: Do nothing
					unset[i] = false;
					refound = true;
					//std::cout << "   -> REFOUND" << std::endl;
					break;
				}
			}

			if (!refound) {
				//std::cout << "   -> NOT FOUND: UNREG" << std::endl;
				// Item not re-found in new vector: Unregister
				elemOld->unregisterElement();
			}
		}

		delete this->elements;
		this->elements = elements;

		for (int i = 0; i < newCount; i++) {
			if (unset[i]) {
				//std::cout << "~ NEWREG: " << std::hex << newIt[i] << std::endl;
				// Item in new compared to old vector: Register
				newIt[i]->registerElement(uiHolder, getOwnPath());
			}
		}

		refreshElememts();
		refreshArrangementCalc();
		updateSelf();


	} else {
		throw std::invalid_argument(
			"Modifying/Reusing the existing element-vector is discouraged! "
			"- Create/Clone instead to create a replacement vector");
	}
}

// Registration

void UIArea::registerElement(UIHolderInterface* uiHolder,
		std::vector<UIElement*>* path) {
	UIElement::registerElement(uiHolder, path);

	for (UIElement* elem : *elements) {
		elem->registerElement(uiHolder, getOwnPath());
	}

}

void UIArea::unregisterElement(bool notify) {
	for (UIElement* elem : *elements) {
		elem->unregisterElement();
	}
	UIElement::unregisterElement(notify);
}


// Events

bool UIArea::pumpEvent(UIEvent* ev) {
	if(UICursorEvent* cursEvent = dynamic_cast<UICursorEvent*>(ev)) {

		if (cursEvent->hasPosition()) {

			int originalX = cursEvent->getCursorX();
			int originalY = cursEvent->getCursorY();

			std::pair<UIElement*, std::vector<int>> found = findElement(originalX, originalY);

			if (found.first != NULL) {

				UICursorEvent* modCursEvent = cursEvent->clone(
						originalX - found.second[0],
						originalY - found.second[1]
						);

				found.first->pumpEvent(modCursEvent);

				delete modCursEvent;
				return true;
			} else {
				return false;
			}

		}

	}
	return false;
}

bool UIArea::pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex) {
	// TODO Pump explicit
	return false;
}

UICommand* UIArea::alignCommand(UICommand* cmd, UIElement* source) {

	if (UIPositionedCommand* posCmd = dynamic_cast<UIPositionedCommand*>(cmd)) {
		if (source != NULL) {
			std::pair<UIElement*, std::vector<int>> found = findElement(source);

			if (found.first != NULL) {

				UIPositionedCommand* modPosCmd = posCmd->clone(
						posCmd->getCursorX() + found.second[0],
						posCmd->getCursorY() + found.second[1]
						);

				return modPosCmd;
			}

		}

	}

	return NULL;
}

void UIArea::pumpCommand(UICommand* cmd, UIElement* source) {

	UICommand* alCmd = alignCommand(cmd, source);

	if (alCmd != NULL) {
		sendUpCommand(alCmd);
		delete alCmd;
	} else {
		sendUpCommand(cmd);
	}
}

void UIArea::notifyUpdate(bool dimensions, UIElement* cause) {
	if (dimensions) {
		refreshSubDimensions();
		refreshArrangementCalc();
	}
	updateSelf();
}

} /* namespace hcuitk */
