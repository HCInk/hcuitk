/*
 * UIDisplayX.h
 *
 *  Created on: Feb 8, 2020
 */

#ifndef HCUITK_DISPLAY_UIDISPLAYX_H_
#define HCUITK_DISPLAY_UIDISPLAYX_H_

#include <X11/Xlib.h>

#include <vector>

#include "../hcuitk.h"



namespace hcuitk {

/* UIEventManager from internal header-file (UIEventManager.h) */
class UIEventManager;

class UIHolderX : public UIHolder {
protected:
	cairo_t* cr;
	DrawingParameters* dp;
	cairo_surface_t* surface;
	UIElement* root = NULL;
	Display* display;
	Drawable window;
	UIEventManager* uievm;
	Atom wmDeleteMessage;

	cairo_surface_t* create_x11_surface(Display* d, Drawable& window, int x, int y);

public:
	UIHolderX(int x, int y);
	virtual ~UIHolderX();

	/* UIHolder Specific*/
	virtual UIElement* getRoot();
	virtual void setRoot(UIElement* root);
	virtual void run();

	/* UIHolderInterface Specific*/
	virtual void subscribeEvent(UIEventType ev, std::vector<UIElement*>* path);
	virtual void unsubscribeEvent(UIEventType ev, UIElement* elem);
	virtual void wipeElement(UIElement* elem);
	virtual void pumpCommand(UICommand* cmd, UIElement* source);
};

} /* namespace hcuitk */

#endif // HCUITK_DISPLAY_UIDISPLAYX_H_
