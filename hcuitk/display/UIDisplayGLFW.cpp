#include "UIDisplayGLFW.hpp"

#include <math.h>
#include <stdio.h>
#include <unistd.h>

#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>

#include "UIEventManager.h"

#include "CairoAgent.cpp"

namespace hcuitk {

UIDisplayGLFW::UIDisplayGLFW(int x, int y) {
    dp = new DrawingParameters(x,y);
    uievm = new UIEventManager();
}

UIDisplayGLFW::~UIDisplayGLFW() {
    delete uievm;
    delete dp;
}


void UIDisplayGLFW::run() {
    	/* Initialize the library */
	if (!glfwInit())
		throw std::runtime_error("Failed to init glfw-library");

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow* window = glfwCreateWindow(dp->getWidth(), dp->getHeight(), "Test", NULL, NULL);
	if (!window) {
		glfwTerminate();
		throw std::runtime_error("Failed to init glfw-window");
	}

	glfwSwapInterval(1);

	int fb_w, fb_h;
	glfwGetFramebufferSize(window, &fb_w, &fb_h);

    CairoAgent agent(window, fb_w, fb_h);

    int version = -1;

	while (!glfwWindowShouldClose(window)) {
        usleep(20000);

        long nversion = root->version();
        if (nversion == -1 || version != nversion) {
            // on framebuffer size change, resize the surface
            glfwGetFramebufferSize(window, &fb_w, &fb_h);
            dp->updateSize(fb_w, fb_h);
            agent.setSize(fb_w, fb_h);

            auto* ctx = agent.beginDraw();

            root->draw(ctx, dp);

            agent.endDraw();

            glfwSwapBuffers(window);

            version = nversion;
        }

		glfwPollEvents();

		// glfwSetWindowShouldClose(window, 1);
	}

	glfwDestroyWindow(window);
	glfwTerminate();
}

UIElement* UIDisplayGLFW::getRoot() {
    return root;
}

void UIDisplayGLFW::setRoot(UIElement* root) {
	if (this->root != nullptr) {
		this->root->unregisterElement(true);
	}
	if (root != nullptr) {
		root->registerElement(this, rootPath);
	}
	this->root = root;
}

void UIDisplayGLFW::subscribeEvent(UIEventType ev, std::vector<UIElement*>* path) {
	uievm->subscribeEvent(ev, path);
}

void UIDisplayGLFW::unsubscribeEvent(UIEventType ev, UIElement* elem) {
	uievm->unsubscribeEvent(ev, elem);
}

void UIDisplayGLFW::wipeElement(UIElement* elem) {
	uievm->wipeElement(elem);
}

void UIDisplayGLFW::pumpCommand(UICommand* cmd, UIElement* source) {

	std::cout << "Recieved Command in Holder" << std::endl;

}


} // namespace hcuitk
