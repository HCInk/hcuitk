/*
 * UIEventManager.cpp
 *
 *  Created on: Feb 8, 2020
 */

#include "UIEventManager.h"

#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <set>
#include <map>

namespace hcuitk {

const UIEventType subscribable[] = {
		UIEventType::Event_CURSOR_PRESS,
		UIEventType::Event_CURSOR_RELEASE,
		UIEventType::Event_KEY_PRESS,
		UIEventType::Event_KEY_RELEASE,
		UIEventType::Event_CURSER_MOTION,
};

UIEventManager::UIEventManager() {

	// Prepare Per-Key subscription entries
	int subscrCount = sizeof(subscribable)/sizeof(*subscribable);
	for (int i = 0; i < subscrCount; i++) {
		std::map<UIEventType, std::map<UIElement*, std::vector<UIElement*>*>*>::iterator it
			= subscriptionsPerEvent.find(subscribable[i]);
		if ((it == subscriptionsPerEvent.end())) {// Hasn't been added yet
			subscriptionsPerEvent[subscribable[i]] = new std::map<UIElement*, std::vector<UIElement*>*>();
		}
	}

}

UIEventManager::~UIEventManager() {
	// Delete per-event subscription-maps
	for (std::pair<const hcuitk::UIEventType, std::map<hcuitk::UIElement*, std::vector<hcuitk::UIElement*>*>*> eventSubscriptionMap : subscriptionsPerEvent) {
		delete eventSubscriptionMap.second;
	}
	subscriptionsPerEvent.clear();
	// Delete per-element subscription-sets
	for (std::pair<const UIElement*, std::set<UIEventType>*> elementSubscriptionSet : subscriptionsPerElement) {
		delete elementSubscriptionSet.second;
	}
	subscriptionsPerElement.clear();
}

void UIEventManager::subscribeEvent(UIEventType ev, std::vector<UIElement*>* path) {
	UIElement* elem = path->back();

	std::map<UIEventType, std::map<UIElement*, std::vector<UIElement*>*>*>::iterator foundSubMap
		= subscriptionsPerEvent.find(ev);

	if (foundSubMap != subscriptionsPerEvent.end()) {// Has a subscription-entry: Is a subscribable event

		{// Add to subscriptionsPerEvent
			std::map<UIElement*, std::vector<UIElement*>*>* subMap = foundSubMap->second;
			std::map<UIElement*, std::vector<UIElement*>*>::iterator foundEntry = subMap->find(elem);

			if (foundEntry != subMap->end()) {// Element already exists: Replace

				// Get previously set entry
				std::vector<hcuitk::UIElement*>* prevSaved = foundEntry->second;

				// Insert new entry with hint
				subMap->insert(foundEntry,
						std::pair<UIElement*, std::vector<UIElement*>*>(elem, path));

				// Delete old entry
				delete prevSaved;

			} else {// Element doesn't exist: Create new entry
				(*subMap)[elem] = path;
			}
		}

		{// Add to subscriptionsPerElement
			std::map<UIElement*, std::set<UIEventType>*>::iterator foundEventSet = subscriptionsPerElement.find(elem);

			// Get set from map
			std::set<UIEventType>* eventSet;
			if (foundEventSet != subscriptionsPerElement.end()) {// Set exists
				// Get pointer from map
				eventSet = foundEventSet->second;
			} else {// Set doesnt exist
				// Create new set
				eventSet = new std::set<UIEventType>();
				subscriptionsPerElement.insert(
						std::pair<UIElement*, std::set<UIEventType>*>(elem, eventSet));
			}

			//Insert event into set
			eventSet->insert(ev);
		}

	} else {// Has a no subscription-entry: Is no a subscribable event
		std::string errorMsg = "Event type ";
		errorMsg += ev;
		errorMsg += " is not subscribable!";
		throw std::invalid_argument(errorMsg);
	}

}

void UIEventManager::unsubscribeEvent(UIEventType ev, UIElement* elem) {

	std::map<UIEventType, std::map<UIElement*, std::vector<UIElement*>*>*>::iterator foundSubMap
		= subscriptionsPerEvent.find(ev);

	if (foundSubMap != subscriptionsPerEvent.end()) {// Has a subscription-entry: Is a subscribable event

		{// Remove from subscriptionsPerEvent
			std::map<UIElement*, std::vector<UIElement*>*>* subMap = foundSubMap->second;
			std::map<UIElement*, std::vector<UIElement*>*>::iterator foundEntry = subMap->find(elem);

			if (foundEntry != subMap->end()) {// Element exists: Remove

				// Get entry-object
				std::vector<hcuitk::UIElement*>* entry = foundEntry->second;

				// Remove entry
				subMap->erase(foundEntry); // Erase by Iterator

				// Delete entry
				delete entry;
			}
		}

		{// Remove from subscriptionsPerElement
			std::map<UIElement*, std::set<UIEventType>*>::iterator foundEventSet
				= subscriptionsPerElement.find(elem);

			if (foundEventSet != subscriptionsPerElement.end()) {// Set exists

				// Get set pointer
				std::set<UIEventType>* eventSet = foundEventSet->second;

				// Erase event from set
				eventSet->erase(ev); //XXX Needs check?

				// Remove if set is empty
				if (eventSet->size() <= 0) {
					// Remove event set entry for element,
					// since the entry doesnt have any more subscribed events

					// Remove entry
					subscriptionsPerElement.erase(foundEventSet); // Erase by iterator

					// Delete entry
					delete eventSet;
				}

			}
		}

	} else {// Has a no subscription-entry: Is no a subscribable event
		std::string errorMsg = "Event type ";
		errorMsg += ev;
		errorMsg += " is not subscribable!";
		throw std::invalid_argument(errorMsg);
	}
}

void UIEventManager::wipeElement(UIElement* elem) {
	std::map<UIElement*, std::set<UIEventType>*>::iterator foundSubscriptions
		= subscriptionsPerElement.find(elem);

	if (foundSubscriptions != subscriptionsPerElement.end()) {//Element has an entry for subscriptions

		// Get subscription-set
		std::set<hcuitk::UIEventType>* subscriptions
			= foundSubscriptions->second;


		// Erase by-event-entries
		for (hcuitk::UIEventType type : *subscriptions) {
			// Get submap (Rely that the sub-map is existent)
			std::map<UIElement*,std::vector<UIElement*>*>* subMap
				= subscriptionsPerEvent.find(type)->second;

			// Find the entry to erase
			std::map<UIElement*,std::vector<UIElement*>*>::iterator foundEntry
				= subMap->find(elem);

			// Erase entry
			if (foundEntry != subMap->end()) {// Element found
				// Get entry-object
				std::vector<hcuitk::UIElement*>* entry = foundEntry->second;

				// Remove entry
				subMap->erase(foundEntry); // Erase by Iterator

				// Delete entry
				delete entry;
			}

		}


		// Delete per-element-entry
		subscriptionsPerElement.erase(foundSubscriptions); // Erase by iterator

		// Delete subscription-set
		delete subscriptions;

	}


}

void UIEventManager::setRootElement(UIElement* root) {
	this->root = root;
}

void UIEventManager::pushExplicites(UIEvent* uiev) {
	hcuitk::UIEventType type = uiev->getType();
	std::map<UIEventType, std::map<UIElement*, std::vector<UIElement*>*>*>::iterator foundSubmap
		= subscriptionsPerEvent.find(type);

	if (foundSubmap != subscriptionsPerEvent.end()) {// Has a subscription-entry: Will evaluate
		std::map<UIElement*, std::vector<UIElement*>*>* submap = foundSubmap->second;

	  for (std::map<UIElement*, std::vector<UIElement*>*>::iterator submapIt = submap->begin(); submapIt!=submap->end(); submapIt++) {
		  //FIXME Concurrency issue, when event subscriptions are changed during iteration
		  std::vector<UIElement*>* path = submapIt->second;
		  //XXX The offset should be 1, shouldn't it?
		  this->root->pumpEventExplicit(uiev, path, 1);
	  }

	}

}

void UIEventManager::pushEvent(UIEvent* uiev) {
	pushExplicites(uiev);
	this->root->pumpEvent(uiev);
}

} /* namespace hcuitk */
