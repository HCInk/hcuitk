/*
 * UIEventManager.h
 *
 *  Created on: Feb 8, 2020
 */

#ifndef HCUITK_DISPLAY_UIEVENTMANAGER_H_
#define HCUITK_DISPLAY_UIEVENTMANAGER_H_

#include <map>
#include <set>
#include <vector>

#include "../hcuitk.h"

namespace hcuitk {

class UIEventManager {
private:
	UIElement* root = NULL;
	/**
	 * 	- K Event Type
	 * 	- V Registered Elements on event (own)
	 * 		- K Pointer to Element subscribed
	 * 		- V Absolute path of the Element subscribed (lent)
	 */
	std::map<UIEventType, std::map<UIElement*,std::vector<UIElement*>*>*> subscriptionsPerEvent;
	std::map<UIElement*, std::set<UIEventType>*> subscriptionsPerElement;

	virtual void pushExplicites(UIEvent* uiev);

public:
	UIEventManager();
	virtual ~UIEventManager();

	virtual void subscribeEvent(UIEventType ev, std::vector<UIElement*>* path);
	virtual void unsubscribeEvent(UIEventType ev, UIElement* elem);
	virtual void wipeElement(UIElement* elem);

	virtual void setRootElement(UIElement* root);
	virtual void pushEvent(UIEvent* uiev);
};

} /* namespace hcuitk */

#endif /* HCUITK_DISPLAY_UIEVENTMANAGER_H_ */
