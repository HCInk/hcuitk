#ifndef HCUITK_DISPLAY_UIDISPLAYGLFW_HPP_
#define HCUITK_DISPLAY_UIDISPLAYGLFW_HPP_

#include "../hcuitk.h"

namespace hcuitk {

/* UIEventManager from internal header-file (UIEventManager.h) */
class UIEventManager;

class UIDisplayGLFW : public UIHolder {
public: 
    class DawingAgent {
    public:
        virtual ~DawingAgent() {}
        virtual void setSize(int width, int height) = 0;
        virtual cairo_t* beginDraw() = 0;
        virtual void endDraw() = 0;
    };
protected:
	DrawingParameters* dp;
	UIEventManager* uievm;
    UIElement* root = nullptr;

public:
	UIDisplayGLFW(int x, int y);
	~UIDisplayGLFW();

	/* UIHolder Specific*/
	UIElement* getRoot() override;
	void setRoot(UIElement* root) override;
	void run() override;

	/* UIHolderInterface Specific*/
	void subscribeEvent(UIEventType ev, std::vector<UIElement*>* path) override;
	void unsubscribeEvent(UIEventType ev, UIElement* elem) override;
	void wipeElement(UIElement* elem) override;
	void pumpCommand(UICommand* cmd, UIElement* source) override;

};

} // namespace hcuitk

#endif // HCUITK_DISPLAY_UIDISPLAYGLFW_HPP_
