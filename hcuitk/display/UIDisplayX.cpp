/*
 * UIDisplayX.cpp
 *
 *  Created on: Feb 8, 2020
 */

#include "UIDisplayX.h"

#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <cairo/cairo-xlib.h>

#include <iostream>
#include <stdexcept>
#include <utility>
#include <vector>

#include "UIEventManager.h"

namespace hcuitk {

cairo_surface_t* UIHolderX::create_x11_surface(Display* d, Drawable& window, int x,
		int y) {
	int screen;
	cairo_surface_t* sfc;

	screen = DefaultScreen(d);
	window = XCreateSimpleWindow(d, DefaultRootWindow(d), 0, 0, x, y, 0, 0, 0);

	XSelectInput(d, window,
			PointerMotionMask |
			ButtonPressMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask
					| StructureNotifyMask);
	XMapWindow(d, window);

	wmDeleteMessage = XInternAtom(d, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(d, window, &wmDeleteMessage, 1);

	XStoreName(d, window, "hcuitk_test");

	sfc = cairo_xlib_surface_create(d, window, DefaultVisual(d, screen), x, y);

	return sfc;
}

UIHolderX::UIHolderX(int x, int y) {

	this->display = XOpenDisplay(NULL);
	if (display == NULL) {
		throw std::runtime_error("Failed to open X-display!");
	}

	this->surface = create_x11_surface(display, window, x, y);
	this->cr = cairo_create(surface);

	this->dp = new DrawingParameters(x, y);
	this->uievm = new UIEventManager();

}

UIHolderX::~UIHolderX() {
	delete dp;
	delete uievm;
	cairo_surface_destroy(surface);
	cairo_destroy(cr);
}

UIElement* UIHolderX::getRoot() {
	return root;
}

void UIHolderX::setRoot(UIElement* root) {
	if (this->root != NULL) {
		this->root->unregisterElement(true);
	}
	if (root != NULL) {
		root->registerElement(this, rootPath);
	}
	this->root = root;
}

void UIHolderX::subscribeEvent(UIEventType ev, std::vector<UIElement*>* path) {
	uievm->subscribeEvent(ev, path);
}

void UIHolderX::unsubscribeEvent(UIEventType ev, UIElement* elem) {
	uievm->unsubscribeEvent(ev, elem);
}

void UIHolderX::wipeElement(UIElement* elem) {
	uievm->wipeElement(elem);
}

void UIHolderX::pumpCommand(UICommand* cmd, UIElement* source) {

	std::cout << "Recieved Command in Holder" << std::endl;

}


void UIHolderX::run() {

	int version = -1;

	int i = 0;

	bool running = true;

	while (true) {
		usleep(20000);

		XEvent event;

		while (XPending(display) > 0) {


			XNextEvent(display, &event);
//			std::cout << "EV " << event.type << std::endl;

			switch (event.type) {
			case ConfigureNotify: {
				std::cout << " ~ CONFIGURE " << std::endl;
				XConfigureEvent evConfigure = event.xconfigure;
				std::cout << " ~ " << evConfigure.width << ":"
						<< evConfigure.height << std::endl;
				cairo_xlib_surface_set_size(surface, evConfigure.width,
						evConfigure.height);
				dp->updateSize(evConfigure.width, evConfigure.height);

				version = -1;
				break;
			}

			case MotionNotify: {
				XMotionEvent evMotion = event.xmotion;

				//std::cout << " ~ MOT " << std::endl;

				hcuitk::UICurserMoveEvent uiev(evMotion.x, evMotion.y);
				root->pumpEvent(&uiev);
				break;
			}

			case ButtonPress: {
				std::cout << " ~ B-PRESS " << std::endl;
				XButtonEvent evButton = event.xbutton;
				std::cout << " ~ CODE:" << evButton.button << std::endl;

				hcuitk::UICurserButtonEvent uiev(
						hcuitk::UIEventType::Event_CURSOR_PRESS,
						evButton.button, evButton.x, evButton.y);
				root->pumpEvent(&uiev);
				break;
			}

			case ButtonRelease: {
				std::cout << " ~ B-RELEASE " << std::endl;
				XButtonEvent evButton = event.xbutton;
				std::cout << " ~ CODE:" << evButton.button << std::endl;

				hcuitk::UICurserButtonEvent uiev(
						hcuitk::UIEventType::Event_CURSOR_RELEASE,
						evButton.button, evButton.x, evButton.y);
				root->pumpEvent(&uiev);
				break;
			}

			case KeyPress: {
				std::cout << " ~ K-PRESS" << std::endl;
				XKeyEvent evKey = event.xkey;
				std::cout << " ~ CODE:" << evKey.keycode << std::endl;
				break;
			}
			case KeyRelease: {
				std::cout << " ~ K-RELEASE" << std::endl;
				XKeyEvent evKey = event.xkey;
				std::cout << " ~ CODE:" << evKey.keycode << std::endl;
				break;
			}

			case ClientMessage: {

		          if ((Atom) event.xclient.data.l[0] == wmDeleteMessage) {
		        	  //std::cout << "CLOSE!" << std::endl;
		        	  running = false;
		        	  break;
		          }
	        	  break;
			}

			default:
				break;
			}

		}

		if (!running) {
			break;
		}

		long nversion = root->version();

		if (nversion == -1 || version != nversion) {
			//std::cout << "RND" << i << std::endl;
			// Clear the background
			cairo_set_source_rgb(cr, 0, 0, 0);
			cairo_paint(cr);

			root->draw(cr, dp);

			cairo_surface_flush(surface);
			XFlush(display);

			//pause for a little bit.
			version = nversion;
			i++;
		}
	}

}

} /* namespace hcuitk */
