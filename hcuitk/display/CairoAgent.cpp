#include "UIDisplayGLFW.hpp"

#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>

#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_X11
#include <GLFW/glfw3native.h>

namespace hcuitk {

class CairoAgent : public UIDisplayGLFW::DawingAgent {
private:
    cairo_t* ctx;
    cairo_surface_t* surface;

public:
    CairoAgent(GLFWwindow* window, int w, int h) {
        Display * x11_display = glfwGetX11Display();
        Window x11_window = glfwGetX11Window(window);
        Visual * vis = DefaultVisual(x11_display, DefaultScreen(x11_display));
        surface = cairo_xlib_surface_create(x11_display, x11_window, vis, w, h);
        ctx = cairo_create(surface);
    }

    ~CairoAgent() {
        cairo_destroy(ctx);
        cairo_surface_finish(surface);
        cairo_surface_destroy(surface);
    }

    void setSize(int width, int height) override {
        cairo_xlib_surface_set_size(surface, width, height);

    }

    cairo_t* beginDraw() override {
        // have to group, else the x server does weird queueing
        cairo_push_group(ctx);
        cairo_set_source_rgb(ctx, 0, 0, 0);
        cairo_paint(ctx);

        return ctx;
    }

    void endDraw() override {
        // ungroup and actually display
        cairo_pop_group_to_source(ctx);
        cairo_paint(ctx);
        cairo_surface_flush(surface);
    }
};


} // namespace hcuitk
