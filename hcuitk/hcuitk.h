/*
 * UIElement.h
 *
 *  Created on: Jan 26, 2020
 */

#ifndef HCUITK_HCUITK_H_
#define HCUITK_HCUITK_H_

#include <cairo/cairo.h>
#include <stdexcept>
#include <utility>
#include <vector>
#include <string>

namespace hcuitk {

enum UIEventType {
	Event_KEY_PRESS = 22,
	Event_KEY_RELEASE = 23,
	Event_CURSOR_PRESS = 24,
	Event_CURSOR_RELEASE = 25,
	Event_CURSER_MOTION = 26,
	Event_CURSER_ENTER = 27,
	Event_CURSER_LEAVE = 28,
};

class UIEvent {
public:
	UIEvent() {
	}
	virtual ~UIEvent() {
	}
	virtual UIEventType getType() = 0;
};

class UICursorEvent : public UIEvent {
private:
	int cursorX;
	int cursorY;
	bool hasPos;
public:
	UICursorEvent(int cursorX, int cursorY) {
		this->cursorX = cursorX;
		this->cursorY = cursorY;
		this->hasPos = true;
	}

	virtual ~UICursorEvent() {
	}

	virtual int getCursorX() {
		return cursorX;
	}
	virtual int getCursorY() {
		return cursorY;
	}

	virtual bool hasPosition() {
		return hasPos;
	}

	virtual UICursorEvent* clone(int cursorX, int cursorY) = 0;

};

class UICurserMoveEvent : public UICursorEvent {
public:
	UICurserMoveEvent(int cursorX, int cursorY) :
			UICursorEvent(cursorX, cursorY) {
	}
	UIEventType getType() {
		return UIEventType::Event_CURSER_MOTION;
	}

	UICurserMoveEvent* clone(int cursorX, int cursorY) {
		return new UICurserMoveEvent(cursorX, cursorY);
	}
};

class UICurserButtonEvent : public UICursorEvent {
private:
	UIEventType evType;
	int button;
public:
	UICurserButtonEvent(UIEventType evType, int button, int cursorX, int cursorY) :
			UICursorEvent(cursorX, cursorY) {
		switch (evType) {
		case UIEventType::Event_CURSOR_PRESS:
		case UIEventType::Event_CURSOR_RELEASE:
			this->evType = evType;
			this->button = button;
			break;
		default:
			std::string errorMsg = "Invalid event type: ";
			errorMsg += std::to_string(evType);
			errorMsg += "! - only BUTTON_PRESS (";
			errorMsg += UIEventType::Event_CURSOR_PRESS;
			errorMsg += ") or BUTTON_RELEASE (";
			errorMsg += UIEventType::Event_CURSOR_RELEASE;
			errorMsg += ") allowed!";
			throw std::invalid_argument(errorMsg);
		}
	}
	virtual ~UICurserButtonEvent() {
	}

	UIEventType getType() {
		return evType;
	}

	UICurserButtonEvent* clone(int cursorX, int cursorY) {
		return new UICurserButtonEvent(evType, button, cursorX, cursorY);
	}

};


enum UICommandType {
	Command_MESSAGE = 0,
	Command_CLOSE = 1,
	Command_CONTEXT_MENU = 2,
	Command_CAPTURE_MOVEMENT = 3,
};

class UICommand {
public:
	UICommand() {}
	virtual ~UICommand() {}
	virtual UICommandType getType() = 0;
};

class UICloseCommand : public UICommand {
public:
	UICloseCommand() {}
	UICommandType getType() {
		return Command_CLOSE;
	}
};

class UIPositionedCommand : public UICommand {
private:
	int cursorX;
	int cursorY;
public:
	UIPositionedCommand(int cursorX, int cursorY) {
		this->cursorX = cursorX;
		this->cursorY = cursorY;
	}

	virtual ~UIPositionedCommand() {
	}

	virtual int getCursorX() {
		return cursorX;
	}
	virtual int getCursorY() {
		return cursorY;
	}

	virtual UIPositionedCommand* clone(int cursorX, int cursorY) = 0;

};

class UIMessageCommand : public UIPositionedCommand {
private:
	std::string msg;
public:
	UIMessageCommand(std::string msg, int cursorX, int cursorY)
		: UIPositionedCommand(cursorX, cursorY) {
		this->msg = msg;
	}
	virtual ~UIMessageCommand() {
	}

	inline std::string getMessage() {
		return msg;
	}

	UICommandType getType() {
		return UICommandType::Command_MESSAGE;
	}

	virtual UIMessageCommand* clone(int cursorX, int cursorY) {
		return new UIMessageCommand(msg, cursorX, cursorY);
	}
};


class DrawingParameters {
private:
	int x;
	int y;
	int width;
	int height;

public:
	DrawingParameters(int width, int height) {
		this->x = 0;
		this->y = 0;
		this->width = width;
		this->height = height;
	}

	DrawingParameters(int x, int y, int width, int height) {
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}

	virtual ~DrawingParameters() {
	}

	inline void updateSize(int width, int height) {
		this->width = width;
		this->height = height;
	}

	inline int getOffX() {
		return x;
	}

	inline int getOffY() {
		return y;
	}

	inline int getWidth() {
		return width;
	}

	inline int getHeight() {
		return height;
	}

};

class UIElement;

class UIHolderInterface {
public:
	virtual ~UIHolderInterface() {
	}

	virtual void subscribeEvent(UIEventType ev, std::vector<UIElement*>* path) = 0;
	virtual void unsubscribeEvent(UIEventType ev, UIElement* elem) = 0;
	virtual void wipeElement(UIElement* elem) = 0;
	virtual void pumpCommand(UICommand* cmd, UIElement* source) = 0;

};

class UIElement {
private:
	std::vector<UIElement*>* ownPath = NULL;
protected:
	UIHolderInterface* uiHolder = NULL;

	inline std::vector<UIElement*>* getOwnPath() {
		return ownPath;
	}

	inline void sendUpdate(bool dimensions) {
		if (ownPath != NULL && ownPath->size() >= 2) {
			UIElement* elem = ownPath->rbegin()[1];
			elem->notifyUpdate(dimensions, this);
		}
	}

	inline void sendUpCommand(UICommand* cmd) {
		if (ownPath != NULL && ownPath->size() >= 2) {
			UIElement* elem = ownPath->rbegin()[1];
			elem->pumpCommand(cmd, this);
		} else if (uiHolder != NULL) {
			uiHolder->pumpCommand(cmd, this);
		}
	}

public:
	virtual ~UIElement() {
		unregisterElement();
	}

	// Core Functions

	/**Draws the UIElement on the given context
	 *
	 * @param cr Cairo-Context to draw on
	 * @param dp DrawingParameters for the draw
	 */
	virtual void draw(cairo_t* cr, DrawingParameters* dp) = 0;

	/**@return The current version of the Object
	 */
	virtual long version() = 0;

	// Registration

	virtual void registerElement(UIHolderInterface* uiHolder, std::vector<UIElement*>* path) {
		if (this->uiHolder != NULL) {
			unregisterElement(true);
		}

		this->uiHolder = uiHolder;
		ownPath = new std::vector<UIElement*>(path->begin(), path->end());
		ownPath->insert(ownPath->end(), this);
	}

	virtual void unregisterElement(bool notify=true) {
		// Unregister from and remove holder
		if (uiHolder != NULL) {
			if (notify) {
				uiHolder->wipeElement(this);
			}
			this->uiHolder = NULL;
		}
		// Delete own path
		if (ownPath != NULL) {
			delete ownPath;
			this->ownPath = NULL;
		}
	}


	// Events

	// Downstream-Events
	/**
	 * @return true if an element was found and the element was delivered
	 */
	virtual bool pumpEvent(UIEvent* ev) = 0;
	/**
	 * @return true if element could be delivered,
	 * 		should always be true if used and executed correctly
	 */
	virtual bool pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex) = 0;
	// Upstream-Events
	virtual void notifyUpdate(bool dimensions, UIElement* cause = NULL) = 0;
	virtual void pumpCommand(UICommand* cmd, UIElement* source) = 0;

	// Properties

	virtual std::pair<int, int>* getMinDimensions() {
		return NULL;
	}

	virtual std::pair<int, int>* getRecomDimensions() {
		return NULL;
	}

	virtual long dimensionVersion() {
		return 0;
	}

};

class UISimpleElement : public UIElement {
	virtual void pumpEvent(UIEvent* ev, bool explicitPush) = 0;

	void notifyUpdate(bool dimensions, UIElement* cause = NULL) {}
	void pumpCommand(UICommand* cmd, UIElement* source) {}

	bool pumpEvent(UIEvent* ev) {
		pumpEvent(ev, false);
		return true;
	}

	bool pumpEventExplicit(UIEvent* ev, std::vector<UIElement*>* path, int pathIndex) {
		pumpEventExplicit(ev, path, pathIndex);
		return true;
	}

};

class UIHolder : public UIHolderInterface {
public:
	std::vector<UIElement*>* rootPath;
	UIHolder() {
		this->rootPath = new std::vector<UIElement*>(0);
	}
	virtual ~UIHolder() {
		delete rootPath;
	}

	virtual UIElement* getRoot() = 0;
	virtual void setRoot(UIElement* root) = 0;

	virtual void run() = 0;
};

} /* namespace hcuitk */

#endif  // HCUITK_HCUITK_H_
