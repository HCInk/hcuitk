#include <iostream>
#include <vector>
#include <utility>

#include "hcuitk.h"
#include "display/UIDisplayGLFW.hpp"
#include "elements/TestUIElement.h"
#include "elements/UIFlexArea.h"
#include "elements/UIOverlayDisplay.h"
#include "elements/UIOverlayHolder.h"

void test() {
	hcuitk::UIDisplayGLFW uiholder(800, 600);

	std::vector<hcuitk::UIElement*>* elements = new std::vector<hcuitk::UIElement*>();

	hcuitk::TestUIElement* elemA = new hcuitk::TestUIElement(
				new std::pair<int, int>(30, 30),
				new std::pair<int, int>(100, 100)
			);
	elements->insert(elements->begin() , elemA);

	hcuitk::TestUIElement* elemB = new hcuitk::TestUIElement(
			new std::pair<int, int>(30, 30),
			new std::pair<int, int>(50, 100)
		);
	elements->insert(elements->begin() , elemB);

	hcuitk::TestUIElement* elemC = new hcuitk::TestUIElement(
			new std::pair<int, int>(30, 30),
			new std::pair<int, int>(100, 100)
		);
	elements->insert(elements->begin() , elemC);

	hcuitk::UIFlexArea* flex = new hcuitk::UIFlexArea(
			elements,
			hcuitk::FlexAreaAlign::FlexAreaAlign_SPACE_BETWEEN,
			hcuitk::FlexAreaDirection::FlexAreaDirection_TOP_BOTTOM
			);

	hcuitk::UIOverlayDisplay* uiod = new hcuitk::UIOverlayDisplay();

	hcuitk::UIOverlayManager* root = new hcuitk::UIOverlayManager(flex, uiod);

	uiholder.setRoot(root);

	uiholder.run();



	// Excample: Close-button adds a new element
	/*
	hcuitk::UIElement* extra = new hcuitk::TestUIElement(new std::pair<int,int>(10, 10), new std::pair<int,int>(10, 10));

	{
		std::vector<hcuitk::UIElement*>* nelements = new std::vector<hcuitk::UIElement*>(*(root->getElements()));

		nelements->insert(nelements->end(), extra);

		root->updateElements(nelements);
	}

	uiholder.run();
	*/

	// Excample: Close-button removes a first element
	/*
	{
		std::vector<hcuitk::UIElement*>* nelements = new std::vector<hcuitk::UIElement*>(*(root->getElements()));

		nelements->erase(nelements->begin());

		root->updateElements(nelements);
	}

	uiholder.run();
	*/

	uiholder.setRoot(NULL);


	delete elemA;
	delete elemB;
	delete elemC;
	//delete extra;
	delete flex;
	delete uiod;
	delete root;

	std::cout << "test finished" << std::endl;

}

int main(int argc, char** argv) {

	test();

	return 0;
}
