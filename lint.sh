#!/bin/bash
files=$(cat .ci/cpplintlist.txt | tr "\n" " ") # Read files to lint
filters=$(grep -o '^[^#]*' .ci/cpplintfilter.txt | tr "\n" ",") # read filters with periods instead of newlines
filters=${filters// } # remove whitespaces
cpplint --filter=$filters $files # lint with given filters
